function countDown(number) {
    console.log(`Count down to ${number}`);

    const newNumber = number - 1;

    if (newNumber > 0) {
        countDown(newNumber);
    } else {
        console.log("Hurra!!!");
    }
}

countDown(5);