# CSS & HTML examples and exercises

This folder contains examples / exercises of css and html.

### Quick links:

* [Backgrounds](https://gitlab.com/vlaats/vali-it-projects/-/tree/master/css-html/backgrounds)
* [Borders](https://gitlab.com/vlaats/vali-it-projects/-/tree/master/css-html/borders)
* [Colors](https://gitlab.com/vlaats/vali-it-projects/-/tree/master/css-html/colors)
* [Flexbox](https://gitlab.com/vlaats/vali-it-projects/-/tree/master/css-html/flexbox)
* [Icons](https://gitlab.com/vlaats/vali-it-projects/-/tree/master/css-html/icons)
* [Lists](https://gitlab.com/vlaats/vali-it-projects/-/tree/master/css-html/lists)
* [Margins](https://gitlab.com/vlaats/vali-it-projects/-/tree/master/css-html/margins)
* [Paddings](https://gitlab.com/vlaats/vali-it-projects/-/tree/master/css-html/paddings)
* [Text](https://gitlab.com/vlaats/vali-it-projects/-/tree/master/css-html/text)
* [Width & Height](https://gitlab.com/vlaats/vali-it-projects/-/tree/master/css-html/width-height)
* [Exercises](https://gitlab.com/vlaats/vali-it-projects/-/tree/master/css-html/exercises)
